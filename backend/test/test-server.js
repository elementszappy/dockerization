process.env.NODE_ENV = 'test';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');

var should = chai.should();

chai.use(chaiHttp);


it('should list ALL Tweets on /tweets GET', function(done) {
    chai.request(server)
        .get('/tweets')
        .end(function(err, res) {
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('array');

            // var tweetsSize = res.body.length;
            // tweetsSize.should.equal(8);

            // res.body[0].should.have.property('_id');
            // res.body[0].should.have.property('text');
            // res.body[0].should.have.property('created_at');

            done();
        });


});