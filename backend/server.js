var http = require('http');
var express = require('express');
var mongoose = require('./mongodb/mongodb.service');
var app = express();
var slack = require('./slack/slack.service');

app.set('port', 3000);
app.use(function(request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

mongoose.connect();
app.get('/', function(req, res) {

    //mongoose.findAllTweets(res);
});
app.get('/tweets', function(req, res) {
    console.log("::::", res)

    mongoose.findAllTweets(res);
});
http.createServer(app).listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
    slack.start();
});

module.exports = app;