const { RtmClient, CLIENT_EVENTS, RTM_EVENTS, WebClient, RTM_MESSAGE_SUBTYPES } = require('@slack/client');
const appData = {};
const slackAppTokenId = 'xoxp-318788468644-319862018151-319445583280-e76f7d4672a6d3dbcd405a8829839a24';
const rtm = new RtmClient(slackAppTokenId, {
    dataStore: false,
    useRtmConnect: true,
});
var twitterService = require('../twitter/twitter.service');

module.exports = {
    start: function() {
        rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED, (connectData) => {
            appData.selfId = connectData.self.id;
            console.log(`Logged in as ${appData.selfId} of team ${connectData.team.id}`);
        });

        rtm.on(CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, () => {
            console.log(`Ready`);
        });

        rtm.on(RTM_EVENTS.MESSAGE, function(message) {
            var messageContent = message.text;
            if (messageContent.indexOf("go") > -1) {
                console.log('New message: ', messageContent);
                twitterService.retrieveTweetsFromFictionFone();
            }
        });

        rtm.start();
    }
}