import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
// declare var $: any;
@Injectable()
export class TweetsService {

  public readonly SERVER_URL = "http://localhost:8000";
  constructor(private http: Http) {
  }

  public readAllTweets(): Observable<any[]> {
    return this.http.get(this.SERVER_URL + '/tweets').map((tweetsData => {
      return tweetsData.json();
    })).catch(err => { return Observable.throw(err); });
  }
}