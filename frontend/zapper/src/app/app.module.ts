import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { TweetsComponent } from './tweets-component/tweets.component'
import { TweetsService } from './services/tweets.service'
import { Http, HttpModule } from '@angular/http';
import {MatCardModule, MatDividerModule,MatListModule} from '@angular/material';
@NgModule({
  declarations: [
    AppComponent,
    TweetsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    MatCardModule,
    MatDividerModule,
    MatListModule
  ],
  providers: [TweetsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
