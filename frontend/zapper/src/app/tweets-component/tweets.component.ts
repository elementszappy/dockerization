import { Component, OnInit } from '@angular/core';
import { TweetsService } from '../services/tweets.service';
@Component({
  selector: 'tweets-component',
  templateUrl: './tweets.component.html'
})
export class TweetsComponent implements OnInit {

  private tweetsData;
  constructor(private tweetsService: TweetsService) {
  }
  ngOnInit() {
    this.tweetsService.readAllTweets().subscribe(
      (data) => this.tweetsData = data,
      (err) => console.log("________;"));
  }
}
